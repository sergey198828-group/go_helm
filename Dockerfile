FROM golang:1.19 AS builder
WORKDIR /app
COPY src/go.mod ./
COPY src/go.sum ./
RUN go mod download
COPY src .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:3.16
RUN addgroup -g 998 nonroot && adduser -S -u 998 -g nonroot nonroot \
    && mkdir /usr/app && chown nonroot:nonroot /usr/app
WORKDIR /usr/app
COPY --from=builder --chown=nonroot:nonroot ./app .
USER 998
EXPOSE 8080
ENTRYPOINT ["./app"]