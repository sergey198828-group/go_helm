from seleniumbase import BaseCase


class finctionalTest(BaseCase):
    def test_main_page(self):
        self.open("https://dev.go-helm.com/test")
        self.assert_text("Here is path parameter test!", "pre")
        self.assert_text("Good color: green", "pre")
        self.assert_text("Bad color: red", "pre")
        self.assert_text("Config file: Yes", "pre")
        self.assert_text("Certificate file: Yes", "pre")

    def test_400_page(self):
        self.open("https://dev.go-helm.com/400/")
        self.assert_text("This is client error stub", "pre")

    def test_500_page(self):
        self.open("https://dev.go-helm.com/500/")
        self.assert_text("This is internal server error stub", "pre")
