// This is a simple echo web server which returns path value
// with additional functionality to check specific env variables
// and configuration files.
// It exposes handlers /400 and /500 to generate http errors.
// All responces have a random poisson distributed delay
package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	xrand "golang.org/x/exp/rand"
	"gonum.org/v1/gonum/stat/distuv"
)

// Metrics define custom values to be exported to Prometheus
type Metrics struct {
	RequestsTotal    *prometheus.CounterVec
	RequestsDuration *prometheus.HistogramVec
}

// Create initializes custom metrics for Prometheus
func (metrics *Metrics) Create() {
	metrics.RequestsTotal = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "go_helm_requests_total",
			Help: "Total number of requests to service",
		},
		[]string{"method", "handler", "code"},
	)

	metrics.RequestsDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "go_helm_requests_duration",
			Help:    "Duration of requests to service",
			Buckets: prometheus.LinearBuckets(0.1, 0.1, 10),
		},
		[]string{"method", "handler", "code"},
	)
	prometheus.MustRegister(metrics.RequestsTotal)
	prometheus.MustRegister(metrics.RequestsDuration)
}

// Log performs a single write of metrics and needs to be called in every handler
func (metrics *Metrics) Log(method string, handler string, code string, duration float64) {
	metrics.RequestsTotal.With(prometheus.Labels{
		"method":  method,
		"handler": handler,
		"code":    code,
	}).Inc()
	metrics.RequestsDuration.With(prometheus.Labels{
		"method":  method,
		"handler": handler,
		"code":    code,
	}).Observe(duration)
}

// generateDelay returns poison distributed random delay with specified lambda
func generateDelay(lambda float64, denominator float64) float64 {
	randSource := xrand.NewSource(uint64(time.Now().UnixNano()))
	poisson := distuv.Poisson{
		Lambda: lambda,
		Src:    randSource,
	}
	return (poisson.Rand() + rand.Float64()) / denominator
}

// handlers is a decorator which returns different handlers for main
func handlers(name string, metrics Metrics) http.HandlerFunc {
	if name == "serverError" {
		// anonymous function to handle 500 error
		return func(w http.ResponseWriter, r *http.Request) {
			randomDelay := generateDelay(2, 4)
			time.Sleep(time.Duration(randomDelay) * time.Second)
			hostname, err := os.Hostname()
			w.WriteHeader(http.StatusInternalServerError)
			if hostname == "" || err != nil {
				hostname = "Undefined"
			}
			fmt.Fprintf(w, "Hello!<br>"+
				"My hostname is: %s.<br>"+
				"This is internal server error stub<br>"+
				"Delay: %f",
				hostname, randomDelay)
			metrics.Log(r.Method, "/500", "500", randomDelay)
		}
	} else if name == "clientError" {
		// anonymous function to handle 400 error
		return func(w http.ResponseWriter, r *http.Request) {
			randomDelay := generateDelay(0, 4)
			time.Sleep(time.Duration(randomDelay) * time.Second)
			hostname, err := os.Hostname()
			w.WriteHeader(http.StatusBadRequest)
			if hostname == "" || err != nil {
				hostname = "Undefined"
			}
			fmt.Fprintf(w, "Hello!<br>"+
				"My hostname is: %s.<br>"+
				"This is client error stub<br>"+
				"Delay: %f",
				hostname, randomDelay)
			metrics.Log(r.Method, "/400", "400", randomDelay)
		}
	} else {
		// anonymous function to handle main page
		return func(w http.ResponseWriter, r *http.Request) {
			randomDelay := generateDelay(1, 4)
			time.Sleep(time.Duration(randomDelay) * time.Second)
			code := "200"
			hostname, err := os.Hostname()
			if hostname == "" || err != nil {
				hostname = "Undefined"
				w.WriteHeader(http.StatusInternalServerError)
				code = "500"
			}
			colorgood := os.Getenv("COLORGOOD")
			colorbad := os.Getenv("COLORBAD")
			confFile := "No"
			if _, err := os.Stat("/etc/goapp/config.yaml"); err == nil {
				confFile = "Yes"
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				code = "500"
			}
			certificateFile := "No"
			if _, err := os.Stat("/etc/ssl/certs/root-ca.pem"); err == nil {
				certificateFile = "Yes"
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				code = "500"
			}
			fmt.Fprintf(w, "Hello!<br>"+
				"My hostname is: %s.<br>"+
				"Here is path parameter %s!<br>"+
				"Good color: %s, Bad color: %s, Config file: %s, Certificate file: %s<br>"+
				"Delay: %f",
				hostname, r.URL.Path[1:], colorgood, colorbad, confFile, certificateFile, randomDelay)
			metrics.Log(r.Method, "/", code, randomDelay)
		}
	}
}

func main() {
	var metrics Metrics
	metrics.Create()
	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/", handlers("main", metrics))
	http.HandleFunc("/400/", handlers("clientError", metrics))
	http.HandleFunc("/500/", handlers("serverError", metrics))
	log.Fatal(http.ListenAndServe(":8080", nil))
}
