# This is example project of deployment simple GO application with Gitlab CI\CD Docker and Helm
Application itself outputs a web page which indicates if specific environment variables COLORGOOD and COLORBAD exist plus checks for a configuration file and ssl certificate files mounted to local container filesystem.

## BUILD:
Run in kubernetes: helm install... -n <namespace>. Please check and edit .helm/values.yaml before then create a namespace. Also you will need to add DNS record according to ingress public IP and provide tls certificates.

## USAGE:
Kubernetes: Open DNS name on port 80 or 443 if you have enabled tls in ingress (by default enabled)

## PIPELINE:
This project has following pipeline setup:
1. On every commit with change to src folder it perform checks for: valid formatting, subtle issues, linting and running unit test cases as well as static application security analisys of go code.
2. On every commit with change to .helm folder it performs static application security analisys for kubernetes manifests.
3. On every commit with change to src folder, Dockerfile or .dockerignote files it does above and rebuilds an image then scans it for vulnerabilities then image pushed to container registry.
4. On every commit to Development branch it deploys application to development kubernetes cluster environment then runs functional tests in chrome browser.
5. On every tag it does all above plus deploys application to production kubernetes cluster environment.