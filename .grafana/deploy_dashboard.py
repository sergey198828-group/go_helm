import requests
import json
import os
import sys


def get_folder(url: str, headers: dict, folder_name: str) -> int:
    """Generates or finds folder for dashboard
    Args:
        headers (dict): http headers with content type and token
        folder_name (str): name of folder to ceate or find
    Raises:
        LookupError: In case of unexpected errors
    Returns:
        int: Id of folder which been created or found
    """
    payload = {
        "title": folder_name
        }
    folder = requests.post(url,
                           headers=headers,
                           data=json.dumps(payload)
                           )
    if folder.status_code == 200:
        return json.loads(folder.content)["id"]
    elif folder.status_code == 409:
        folders = requests.get(url,
                               headers=headers
                               )
        for folder in json.loads(folders.content):
            if folder["title"] == folder_name:
                return folder["id"]
    else:
        raise LookupError(
            f"Unable to lookup/create folder {folder.status_code}"
        )


def import_dashboard(url: str,
                     headers: dict,
                     folder_id: int,
                     dashboard_file: str,
                     dashboard_comment: str) -> None:
    """Imports Grafana dashboard from specified file to specified url
    Args:
        url (str): _description_
        headers (dict): _description_
        folder_id (int): _description_
        dashboard_file (str): _description_
    Raises:
        ValueError: _description_
    """
    with open(dashboard_file, "r", encoding="utf-8") as file:
        dashboard = json.load(file)
    payload = {
        "dashboard": dashboard,
        "folderId": folder_id,
        "message": dashboard_comment,
        "overwrite": True
    }
    result = requests.post(url, headers=headers, data=json.dumps(payload))
    print(result)


if __name__ == "__main__":
    token = os.getenv("GRAFANA_TOKEN")
    if not token:
        raise ValueError(
            "Grafana token not found, set GRAFANA_TOKEN environment variable"
        )
    app_name = os.getenv("APP_NAME")
    if not app_name:
        raise ValueError(
            "Application name not found, set APP_NAME environment variable"
        )
    try:
        grafana_url = sys.argv[1]
    except IndexError:
        raise ValueError(
            "Please provide grafana url as first command line parameter"
        )
    try:
        dashboard_file = sys.argv[2]
    except IndexError:
        raise ValueError(
            "Please provide dashboard file as second command line parameter"
        )
    try:
        dashboard_comment = sys.argv[3]
    except IndexError:
        raise ValueError(
            "Please provide dashboard comment as third command line parameter"
        )
    headers = {
        "Authorization": f"Bearer {token}",
        "Accept": "application/json",
        "Content-Type": "application/json"
    }
    folder_id = get_folder(f"{grafana_url}/api/folders",
                           headers,
                           app_name
                           )
    import_dashboard(f"{grafana_url}/api/dashboards/db",
                     headers,
                     folder_id,
                     dashboard_file,
                     dashboard_comment
                     )
